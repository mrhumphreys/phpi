<?php
/**
* A PHP class written to simplify interacting with the
* General Purpose Input/Ouput on a Raspberry Pi B+
*
* @author Cameron Montgomery
*
*/
class gpioBplus extends gpioB {

protected $_valid_gpio = array(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27);
    
/**
* Exports the specified gpio and optionally sets the direction, gpio base directory
* and whether to unexport on destruct.
*
* @param	int	$gpio   the Raspberry Pi gpio number, not the pin number.
* @param	string	$direction  in, out, or NULL to not specify the directory
* @param	bool	$unexport   set whether to unexport the gpio on dectruct
* @param	string	$basedir    the location of the gpio files. defaults to /sys/class/gpio/
*/
public function __construct($gpio, $direction = NULL, $unexport = true, $basedir = NULL) {
    parent::__construct($gpio, $direction = NULL, $unexport = true, $basedir = NULL);
}


public function __destruct() {
    parent::__destruct();
}


public function __toString() {
    parent::__toString();
}


}
?>