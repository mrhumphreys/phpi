#!/usr/bin/php
<?php
require_once("gpioB.php");

$gpios = array();
$actions = array("get", "blink", "in", "off", "on", "out", "toggle", "quit");

pcntl_signal(SIGINT, "sig_handler");

echo usage($actions);

while(true) {
	echo "Action: ";
	$args = fgetcsv(STDIN, 1024, " ");
	$action = "";

	for($i = 0; $i < count($args); $i++) {
		if($i == 0) {
			if(!in_array(strtolower($args[$i]), $actions)) {
				echo "First argument must be one of:" . chr(10) . implode(",", $actions) . chr(10);
				break;
			}

			if(strtolower($args[$i]) == "quit") {
				foreach($gpios as $gpio) {
					unset($gpio);
				}
				exit(1);

			}
                        
                        if(strtolower($args[$i]) == "blink") {
                            $pattern = "/\{(\d+),(\d{0,2}(?:\.?\d+)?),(\d{0,2}(?:\.?\d+)?)\}/";
                            $j = $i + 2;
                            do {
                                if(!@preg_match($pattern, $args[$j])) {
                                    echo "Argument 'blink' must be followed by " . chr(10) . "<gpio> {<num_blinks>,<on_secs>,<off_secs>}" . chr(10) . chr(10);
                                    break 2;
                                }
                                $j += 2;
                            } while ($j <= $i);
                        }
			
                        $action = strtolower($args[$i]);
			continue;
		}
		try {
			switch(strtolower($action)) {
                            case "blink":
                                /*
                                 * To do: Add ipc so parent process can term child processes via a stop action
                                 */
                                preg_match($pattern, $args[$i + 1], $matches);
                                $pid = pcntl_fork();
                                
                                switch($pid) {
                                    case -1:
                                        throw new Exception("Unable to fork blink process");
                                        
                                    case 0:
                                        $num = $matches[1];
                                        $on = $matches[2];
                                        $off = $matches[3];
                                        for($x = 0; $x < $num; $x++) {
                                            if(!in_array($args[$i], array_keys($gpios))) {
                                                $gpios[$args[$i]] = new gpioB($args[$i], "out", false);
                                                $gpios[$args[$i]]->gpio_ctl = true;
                                            }
                                            
                                            $gpios[$args[$i]]->on();
                                            usleep($on * 1000000);
                                            $gpios[$args[$i]]->off();
                                            usleep($off * 1000000);
                                        }
                                        exit();
                                    default:
                                        $i++;
                                }
                                break;
                                
                            case "get":
                                if(!in_array($args[$i], array_keys($gpios))) {
                                        $gpios[$args[$i]] = new gpioB($args[$i], NULL, false);
                                        $gpios[$args[$i]]->gpio_ctl = false;
                                }
                                break;

                            case "off":
                                if(!in_array($args[$i], array_keys($gpios))) {
                                        $gpios[$args[$i]] = new gpioB($args[$i], "out");
                                        $gpios[$args[$i]]->gpio_ctl = true;
                                }
                                $gpios[$args[$i]]->off();
                                break;

                            case "on":
                                if(!in_array($args[$i], array_keys($gpios))) {
                                        $gpios[$args[$i]] = new gpioB($args[$i], "out");
                                        $gpios[$args[$i]]->gpio_ctl = true;
                                }
                                $gpios[$args[$i]]->on();
                                break;
                                                case "toggle":
                                if(!in_array($args[$i], array_keys($gpios))) {
                                        $gpios[$args[$i]] = new gpioB($args[$i], "out");
                                        $gpios[$args[$i]]->gpio_ctl = true;
                                }
                                $gpios[$args[$i]]->toggle();
                                break;

                            case "in":
                            case "out":
                                if(!in_array($args[$i], array_keys($gpios))) {
                                        $gpios[$args[$i]] = new gpioB($args[$i], $action);
                                        $gpios[$args[$i]]->gpio_ctl = true;
                                }
                                $gpios[$args[$i]]->set_direction($action);
                                break;
			}
		} catch(exception $e) {
			echo $e->getMessage() . chr(10);
		}	
	}

	ksort($gpios);
	foreach($gpios as $gpio) {
		echo str_repeat("-", 24) . chr(10);
		printf("gpio:      %12s\n", $gpio);
		printf("direction: %12s\n", $gpio->get_direction());
		printf("state:     %12s\n", $gpio->get_state());
		printf("gpio_ctl:  %12s\n", $gpio->gpio_ctl);

	}
	echo str_repeat("-", 24) . chr(10) . chr(10);

        pcntl_signal_dispatch();
}

function usage($actions) {
$actions = implode(", ", $actions);
$string =<<<EOT
Usage: ACTION GPIO [options] [GPIO] [options]...
Where ACTION is one of:
$actions
blink options: {<num_blinks>,<on_secs>,<off_secs>}

        
EOT;
echo $string;
}

function sig_handler($signo) {
    switch($signo) {
        case SIGCHLD:
            if(pcntl_waitpid(0, $status, WNOHANG) != 0) {
                echo "Flash ended" . chr(10);
            }
            break;
    }
}
