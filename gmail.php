#!/usr/bin/php
<?php

require_once("gpio.php");
pcntl_signal(SIGINT, "sig_handler");
define("WAIT", 5);
try {
	if(!isset($argv[1])) exit("Must supply a gpio pin\n");

	$gpio = new gpio($argv[1], "out");

        $config = parse_ini_file("gmail.ini", true);
        
	$key = base64_decode("wcveJ2SOONex8tdkrj12hw==");
	$key_size = 16;
	$user_decrypt = base64_decode($config["options"]["user_key"]);
	$pass_decrypt = base64_decode($config["options"]["pass_key"]);

	$user_iv_dec = substr($user_decrypt, 0, $key_size);
	$pass_iv_dec = substr($pass_decrypt, 0, $key_size);

	$user_decrypt = substr($user_decrypt, $key_size);
	$pass_decrypt = substr($pass_decrypt, $key_size);

	$user = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $user_decrypt, MCRYPT_MODE_CBC, $user_iv_dec);
	$pass = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $pass_decrypt, MCRYPT_MODE_CBC, $pass_iv_dec);

	unset($key);
	unset($key_size);
	unset($user_decrypt);
	unset($user_iv_dec);
	unset($pass_decrypt);
	unset($pass_iv_dec);

	$url = 'https://' . trim($user) . ':' . trim($pass) . '@mail.google.com/mail/feed/atom/';
	while(true) {
		$feed = new SimpleXMLElement($url, NULL, true);
		if(intval($feed->fullcount) > 0) {
			$gpio->on();
		} else {
			$gpio->off();
		}
		unset($feed);
		pcntl_signal_dispatch();
		sleep(WAIT * 60);
	}
} catch(exception $e) {
	echo $e->getMessage() . chr(10);
}

function sig_handler($signo) {
	global $gpio;

	switch($signo) {
		case SIGINT:
			echo "Exiting...\n";
			try {
				unset($gpio);
			} catch(exception $e) {
				$e->getMessage();
			}
			exit();
	}
}
?>
