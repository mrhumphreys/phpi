#!/usr/bin/php
<?php
require_once("gpio.php");

define("DIT", "250000");
define("DAH", DIT * 3);
define("SPACE", DIT * 7);

if(!isset($argv[1]) && !isset($argv[2])) {
	exit('Usage: morse.php GPIO "MORSE TEXT"' . chr(10));
}

$string = trim(strtoupper($argv[2]));

$morse = array(
	"A" => array(DIT, DAH),
	"B" => array(DAH, DIT, DIT, DIT),
	"C" => array(DAH, DIT, DAH, DIT),
	"D" => array(DAH, DIT, DIT),
	"E" => array(DIT),
	"F" => array(DIT, DIT, DAH, DIT),
	"G" => array(DAH, DAH, DIT),
	"H" => array(DIT, DIT, DIT, DIT),
	"I" => array(DIT, DIT),
	"J" => array(DIT, DAH, DAH, DAH),
	"K" => array(DAH, DIT, DAH),
	"L" => array(DIT, DAH, DIT, DIT),
	"M" => array(DAH, DAH),
	"N" => array(DAH, DIT),
	"O" => array(DAH, DAH, DAH),
	"P" => array(DIT, DAH, DAH, DIT),
	"Q" => array(DAH, DAH, DIT, DAH),
	"R" => array(DIT, DAH, DIT),
	"S" => array(DIT, DIT, DIT),
	"T" => array(DAH),
	"U" => array(DIT, DIT, DAH),
	"V" => array(DIT, DIT, DIT, DAH),
	"W" => array(DIT, DAH, DAH),
	"X" => array(DAH, DIT, DIT, DAH),
	"Y" => array(DAH, DIT, DAH, DAH),
	"Z" => array(DAH, DAH, DIT, DIT),
	"1" => array(DIT, DAH, DAH, DAH, DAH),
	"2" => array(DIT, DIT, DAH, DAH, DAH),
	"3" => array(DIT, DIT, DIT, DAH, DAH),
	"4" => array(DIT, DIT, DIT, DIT, DAH),
	"5" => array(DIT, DIT, DIT, DIT, DIT),
	"6" => array(DAH, DIT, DIT, DIT, DIT),
	"7" => array(DAH, DAH, DIT, DIT, DIT),
	"8" => array(DAH, DAH, DAH, DIT, DIT),
	"9" => array(DAH, DAH, DAH, DAH, DIT),
	"0" => array(DAH, DAH, DAH, DAH, DAH),
	" " => array(SPACE)
	);

try {
	$gpio = new gpio($argv[1], "out");

	echo "'$string' in morse code is:\n";
	foreach(str_split($string) as $letter) {
		if(!array_key_exists($letter, $morse)) {
			echo "Morse code for '" . $letter . "' has not been set" . chr(10);
			continue;
		}
		echo "$letter:" . implode(",", $morse[$letter]) . chr(10);
		foreach($morse[$letter] as $code) {
			if($letter == " ") {
				usleep(SPACE - DAH - DIT);
			} else {
				$gpio->on();
				usleep($code);
				$gpio->off();
				usleep(DIT);
			}
		}
		usleep(DAH - DIT);
	}

} catch(exception $e) {
	echo $e->getMessage() . chr(10);
}

unset($gpio);
?>
