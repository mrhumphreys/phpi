<?php
/**
* A PHP class written to simplify interacting with the
* General Purpose Input/Ouput on a Raspberry Pi B
*
* @author Cameron Montgomery
*
*/
class gpioB {

protected $_basedir = "/sys/class/gpio";
protected $_direction_file;
protected $_gpio;
protected $_gpio_dir;
protected $_raw_gpio;
protected $_unexport;
protected $_value_file;
protected $_valid_gpio = array(0, 1, 2, 3, 4, 7, 8, 9, 10, 11,
	 14, 15, 17, 18, 21, 22, 23, 24, 25, 27);

/**
* Exports the specified gpio and optionally sets the direction, gpio base directory
* and whether to unexport on destruct.
*
* @param	int	$gpio   the Raspberry Pi gpio number, not the pin number.
* @param	string	$direction  in, out, or NULL to not specify the directory
* @param	bool	$unexport   set whether to unexport the gpio on dectruct
* @param	string	$basedir    the location of the gpio files. defaults to /sys/class/gpio/
*/
public function __construct($gpio, $direction = NULL, $unexport = true, $basedir = NULL) {
	$direction = $direction === NULL ? NULL : strtolower($direction);
	if(!in_array(intval($gpio), $this->_valid_gpio)) {
		throw new exception("gpio received: $gpio. Must be one of: " . implode(",", $this->_valid_gpio));
	}

	$this->_raw_gpio = $gpio;
	$this->_gpio = "gpio" . $gpio;

	if($basedir !== NULL) {
            if(!is_dir($basedir)) {
                throw new exception("$basedir is not a directory");
            }
            $this->_basedir = $basedir;
        }

	$this->_gpio_dir = $this->_basedir . "/" . $this->_gpio;
	$this->_direction_file = $this->_gpio_dir . "/direction";
	$this->_value_file = $this->_gpio_dir . "/value";

	$this->_export();

	if($direction !== NULL) {
		if(!$this->set_direction($direction)) {
			throw new exception($this->_gpio . ": Unable to set direction");
		}
	}

	$this->_unexport = $unexport == true  ? true : false;
}

/**
* Gets the current direction of the gpio
*
* @return	string
*/
public function get_direction() {
	try {
		$fh = new SPLFileObject($this->_direction_file, 'r');
		$return = $fh->fgets();
		unset($fh);
		return trim($return);
	} catch(exception $e) {
		throw new exception($this->_gpio . ":" . __METHOD__ . ": " . $e->getMessage());
	}
}

/**
* Gets the current state of the gpio (on = 1 |off = 0)
*
* @return	int
*/
public function get_state() {
	$fh = new SPLFileObject($this->_value_file, "r");
	$return = $fh->fgets();
	unset($fh);
	return trim(intval($return));
}

/**
* Sets the gpio state to off (0)
*/
public function off() {
	if($this->get_direction() != "out") {
		throw new exception($this->_gpio . ":" . __METHOD__ . ": Unable to set state (off), direction is not out");
	}
	if(!$this->_set_value(0)) {
		throw new exception($this->_gpio . ":" . __METHOD__ . ": Unable to set state (off)");
	}
}

/**
* Sets the gpio state to on (1)
*/
public function on() {
	if($this->get_direction() != "out") {
		throw new exception($this->_gpio . ":" . __METHOD__ . ": Unable to set state (on), direction is not out");
	}

	if(!$this->_set_value(1)) {
		throw new exception($this->_gpio . ":" . __METHOD__ . ": Unable to set state (on)");
	}
}

/**
* Sets the direction of the gpio
*
* @param	string	$direction  the direction of the gpio. in or out
* @return	bool
*/
public function set_direction($direction) {
	$direction = strtolower($direction);
	if(($direction != "in") && ($direction != "out")) {
		throw new exception($this->_gpio . ": direction received <$direction>. Must be 'in' or 'out'.");
	}

	$fh = new SPLFileInfo($this->_direction_file);
	$i = 0;
	while($fh->isFile() == false) {
		usleep(3000);
		if($i > 30) {
			unset($fh);
			return false;
		}
		$i++;
	}

	while($fh->isWritable() == false) {
		usleep(1000);
	}

	$fh = $fh->openFile("w");
	$fh->fwrite($direction);
	unset($fh);

	return true;
}

/**
* Toggles the state of the gpio based on the current state
*
* @return	bool
*/
public function toggle() {
	if($this->get_direction() != "out") {
		throw new exception($this->_gpio . ': Unable to toggle, direction is not out');
	}

	$value = $this->get_state();

	switch($value) {
		case 1:
			$this->off();
			break;
                case 0:
		default:
			$this->on();
	}

	return true;
}

public function __toString() {
	return $this->_gpio;
}

public function __destruct() {
	if($this->get_direction() == "out") $this->off();
	if($this->_unexport) $this->_unexport();
}

/**
* Exports the gpio if needed.
*
* @return	bool
*/
protected function _export() {
	$fh = new SPLFileInfo($this->_gpio_dir);
	if($fh->isLink()) {
		unset($fh);
		return true;
	}

	try {
		$fe = new SPLFileObject($this->_basedir . "/export", "w");
		$fe->fwrite($this->_raw_gpio);
		unset($fe);
	} catch(exception $e) {
		return false;
	}
        
	while($fh->isLink() == false) {
		usleep(1000);
	}

	unset($fh);
	return true;
}

/**
* Sets the value of the gpio
*
* @param	int	$value  0 or 1
* @return	bool
*/
protected function _set_value($value) {
	try {
		$fh = new SPLFileObject($this->_value_file, "w");
		$fh->fwrite($value);
		unset($fh);
	} catch(exception $e) {
		return false;
	}
	return true;
}

/**
* Unexports the gpio
*
* @return	bool
*/
protected function _unexport() {
	try {
		$fh = new SPLFileObject($this->_basedir . "/unexport", "w");
		$fh->fwrite($this->_raw_gpio);
		unset($fh);
	} catch(exception $e) {
		return false;
	}
        
	return true;
}

}
?>
